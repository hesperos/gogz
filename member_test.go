package gogz

import (
	"compress/gzip"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestIfItsPossibleToObtainFileHeader(t *testing.T) {
	inputFh, err := os.Open("testdata/rfc1952.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	if member == nil {
		t.Fatalf("current member is nil\n")
	}

	if member.Header.FileName != "rfc1952.txt" {
		t.Fatalf("invalid filename: [%s]\n", member.Header.FileName)
	}

	if member.Header.CompressionMethod != CmDeflate {
		t.Fatalf("invalid compression-method: %d\n", member.Header.CompressionMethod)
	}

	if member.Header.Os != OsUnix {
		t.Fatalf("invalid os %d\n", member.Header.Os)
	}
}

func TestIfFileContainsAFixedBlock(t *testing.T) {
	inputFh, err := os.Open("testdata/hw.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	blockType, isFinal, err := readBlockHeader(member.bitReader)
	if err != nil {
		t.Fatalf("error when reading block header: %s\n", err)
	}

	if blockType != BlockCompressedFixed {
		t.Fatalf("expected a unique fixed compressed block, got: %v\n", blockType)
	}

	if isFinal != true {
		t.Fatalf("expected a unique final block, got: %v\n", isFinal)
	}
}

func TestIfItsPossibleToReadSimpleFixedBlocks(t *testing.T) {
	inputFh, err := os.Open("testdata/hw.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	expected := "hello, world"
	got := make([]byte, len(expected)*2)

	n, err := member.Read(got)
	if err != nil {
		t.Fatalf("unexpected error: %v\n", err)
	}

	if n != len(expected) {
		t.Fatalf("got length: %d, expected: %d\n", n, len(expected))
	}

	gotStr := strings.TrimRight(string(got), "\x00")
	if gotStr != expected {
		t.Fatalf("got: [%s], expected: [%s]\n", gotStr, expected)
	}
}

func TestIfReportsCrcErrorsForCorruptedFiles(t *testing.T) {
	inputFh, err := os.Open("testdata/hw_corrupted_crc32.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	got := make([]byte, 1024)
	n, err := member.Read(got)
	if err == nil {
		t.Fatalf("expected crc error, got nothing\n")
	}

	if n != 0 {
		t.Fatalf("expected zero length read, got %d\n", n)
	}
}

func decompress(gzFile string) ([]byte, error) {
	inputFh, err := os.Open(gzFile)
	if err != nil {
		return []byte{}, err
	}
	defer inputFh.Close()

	r, err := gzip.NewReader(inputFh)
	if err != nil {
		return []byte{}, err
	}

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return []byte{}, err
	}

	return data, nil
}

func TestIfIsAbleToReplicateRunLengthSequences(t *testing.T) {
	testRecords := []struct {
		GzFile string
	}{
		{"testdata/echo3.gz"},
		{"testdata/a128bdef.gz"},
	}

	for _, testRecord := range testRecords {
		inputFh, err := os.Open(testRecord.GzFile)
		if err != nil {
			t.Fatalf("unable to open test asset: %s\n", err)
		}
		defer inputFh.Close()

		// Decompress using production quality implementation in order to
		// obtain a reference data for comparison.
		expectedData, err := decompress(testRecord.GzFile)

		backBuffer, err := NewBackBuffer(32768)
		if err != nil {
			t.Fatalf("unable to instantiate backbuffer: %s\n", err)
		}

		member, err := NewMember(inputFh, backBuffer)
		if err != nil {
			t.Fatalf("error when reading member: %s\n", err)
		}

		gotData, err := ioutil.ReadAll(member)
		if err != nil {
			t.Fatalf("error when reading data: %s\n", err)
		}

		if len(expectedData) != len(gotData) {
			t.Fatalf("decompressed data length mismatch. got: %d, expected %d\n",
				len(gotData),
				len(expectedData))
		}

		for i, _ := range gotData {
			if gotData[i] != expectedData[i] {
				t.Fatalf("discrepant data @%d. got: [%x] expected: [%d]\n",
					i,
					gotData[i],
					expectedData[i])
			}
		}
	}

}

func TestIfIsAbleToDealWithFilesContainingDynamicBlocks(t *testing.T) {
	testRecords := []struct {
		GzFile string
	}{
		{"testdata/rfc1951.txt.gz"},
		{"testdata/rfc1952.txt.gz"},
	}

	for _, testRecord := range testRecords {
		inputFh, err := os.Open(testRecord.GzFile)
		if err != nil {
			t.Fatalf("unable to open test asset: %s\n", err)
		}
		defer inputFh.Close()

		// Decompress using production quality implementation in order to
		// obtain a reference data for comparison.
		expectedData, err := decompress(testRecord.GzFile)

		backBuffer, err := NewBackBuffer(65535)
		if err != nil {
			t.Fatalf("unable to instantiate backbuffer: %s\n", err)
		}

		member, err := NewMember(inputFh, backBuffer)
		if err != nil {
			t.Fatalf("error when reading member: %s\n", err)
		}

		gotData, err := ioutil.ReadAll(member)
		if err != nil {
			t.Fatalf("error when reading data: %s\n", err)
		}

		if len(expectedData) != len(gotData) {
			t.Fatalf("decompressed data length mismatch. got: %d, expected %d\n",
				len(gotData),
				len(expectedData))
		}

		for i, _ := range gotData {
			if gotData[i] != expectedData[i] {
				t.Fatalf("discrepant data @%d. got: [%x] expected: [%d]\n",
					i,
					gotData[i],
					expectedData[i])
			}
		}
	}

}

func TestIfIsAbleToDealWithFilesContainingUncompressedBlocks(t *testing.T) {
	testRecords := []struct {
		GzFile string
	}{
		{"testdata/random1k_01.gz"},
		{"testdata/random1k_02.gz"},
	}

	for _, testRecord := range testRecords {
		inputFh, err := os.Open(testRecord.GzFile)
		if err != nil {
			t.Fatalf("unable to open test asset: %s\n", err)
		}
		defer inputFh.Close()

		// Decompress using production quality implementation in order to
		// obtain a reference data for comparison.
		expectedData, err := decompress(testRecord.GzFile)

		backBuffer, err := NewBackBuffer(65535)
		if err != nil {
			t.Fatalf("unable to instantiate backbuffer: %s\n", err)
		}

		member, err := NewMember(inputFh, backBuffer)
		if err != nil {
			t.Fatalf("error when reading member: %s\n", err)
		}

		gotData, err := ioutil.ReadAll(member)
		if err != nil {
			t.Fatalf("error when reading data: %s\n", err)
		}

		if len(expectedData) != len(gotData) {
			t.Fatalf("decompressed data length mismatch. got: %d, expected %d\n",
				len(gotData),
				len(expectedData))
		}

		for i, _ := range gotData {
			if gotData[i] != expectedData[i] {
				t.Fatalf("discrepant data @%d. got: [%x] expected: [%d]\n",
					i,
					gotData[i],
					expectedData[i])
			}
		}
	}
}

func BenchmarkDynamicBlockDecompression(b *testing.B) {
	testRecords := []struct {
		GzFile string
	}{
		{"testdata/rfc1951.txt.gz"},
		{"testdata/rfc1952.txt.gz"},
	}

	backBuffer, _ := NewBackBuffer(65535)

	for n := 0; n < b.N; n++ {
		for _, testRecord := range testRecords {
			inputFh, _ := os.Open(testRecord.GzFile)
			defer inputFh.Close()

			member, _ := NewMember(inputFh, backBuffer)
			ioutil.ReadAll(member)
		}
	}
}

func BenchmarkProductionDecompressor(b *testing.B) {
	testRecords := []struct {
		GzFile string
	}{
		{"testdata/rfc1951.txt.gz"},
		{"testdata/rfc1952.txt.gz"},
	}

	for n := 0; n < b.N; n++ {
		for _, testRecord := range testRecords {
			decompress(testRecord.GzFile)
		}
	}
}
