package gogz

import (
	"math/bits"
)

type HuffmanCode uint32

// type HuffmanCode struct {
// 	Code   uint16
// 	Length uint8
// }

type Symbol uint16

type HuffmanLookup map[HuffmanCode]Symbol

func findMaxKey(m map[uint8]int) uint8 {
	var maxKey uint8 = 0
	for i, _ := range m {
		if i > maxKey {
			maxKey = i
		}
	}
	return maxKey
}

// Transforms a CodeLengthList (cll[symbol] = code-length) to a
// HuffmanLookup map.
func GeneratHuffmanLookupFromCodeLengthList(cList CodeLengthList) HuffmanLookup {
	lookup := make(map[HuffmanCode]Symbol, len(cList))
	codesOfLen := make(map[uint8]int)

	// codesOfLen will contain counters for every code-length
	// i.e.: codeOsLen[5] = 10 - code of length 5 occurred 10 times
	for _, codeLen := range cList {
		if codeLen != 0 {
			codesOfLen[codeLen] += 1
		}
	}

	maxLen := findMaxKey(codesOfLen)

	// find numeric value of the smallest code of given length
	currentCode := 0
	nextCode := make([]int, maxLen+1)
	for bits := 1; bits <= int(maxLen); bits++ {
		currentCode = (currentCode + codesOfLen[uint8(bits-1)]) << 1
		nextCode[bits] = currentCode
	}

	for symbol, codeLen := range cList {
		if codeLen > 0 {
			// hc := HuffmanCode{uint16(nextCode[codeLen]), codeLen}
			hc := uint32(codeLen)<<16 | uint32(nextCode[codeLen])
			lookup[HuffmanCode(hc)] = Symbol(symbol)
			nextCode[codeLen] += 1
		}
	}

	return lookup
}

func getMinMaxHuffmanCodeLength(hl HuffmanLookup) (int, int) {
	minLen, maxLen := 255, 0

	for c, _ := range hl {
		// if int(c.Length) > maxLen {
		// 	maxLen = int(c.Length)
		// }
		//
		// if int(c.Length) < minLen {
		// 	minLen = int(c.Length)
		// }
		if int(c>>16) > maxLen {
			maxLen = int(c >> 16)
		}

		if int(c>>16) < minLen {
			minLen = int(c >> 16)
		}
	}
	return minLen, maxLen
}

func reverseBits(code uint64, n uint) uint64 {
	if n == 1 {
		return code
	}

	reversed := bits.Reverse64(code)
	return reversed >> (64 - n)
}

func matchHuffmanCode(b *BitReader, hl HuffmanLookup, minLen, maxLen int) (Symbol, error) {
	var symbol Symbol = 0
	var completeCode uint16 = 0
	var completeLen uint8 = 0
	toRead := uint(minLen)

	for completeLen <= uint8(maxLen) {
		code, err := b.ReadBits(toRead)
		if err != nil {
			return 0, err
		}
		code = reverseBits(code, toRead)
		completeCode <<= toRead
		completeCode |= uint16(code)
		completeLen += uint8(toRead)

		// hc := HuffmanCode{completeCode, completeLen}
		hc := HuffmanCode(uint32(completeLen)<<16 | uint32(completeCode))
		if symbol, keyExists := hl[hc]; keyExists {
			return symbol, nil
		} else {
			toRead = 1
		}
	}

	return symbol, nil
}
