package gogz

// Describes a range of symbols from original alphabet which have been assigned
// a Huffman code of given length.
type FixedCodeSymbolRange struct {
	// Defines the first value of the symbol range
	StartSymbol Symbol

	// Defines the number of symbols encoded with given code-length
	Symbols uint8

	// Length of code for the symbol range from the original alphabet
	CodeLength uint8
}

func (f FixedCodeSymbolRange) LastSymbol() Symbol {
	return Symbol(uint16(f.StartSymbol) + uint16(f.Symbols))
}

// Returns the definitions of fixed Huffman tables as described in rfc1951 for
// DEFLATE compression algorithm for literal/length alphabet.
func getFixedCodeLitLenSymbolRanges() []FixedCodeSymbolRange {
	return []FixedCodeSymbolRange{
		{0, 144, 8},
		{144, 122, 9},
		{256, 24, 7},
		{280, 8, 8},
	}
}

// Returns the definitions of fixed Huffman tables as described in rfc1951 for
// DEFLATE compression algorithm for distance alphabet.
func getFixedCodeDistanceSymbolRanges() []FixedCodeSymbolRange {
	// distance alphabet for fixed blocks is comprised of 5-bit codes
	return []FixedCodeSymbolRange{
		{0, 32, 5},
	}
}

// Represents a Huffman code length list. Indexes are symbols and the values
// represent a Huffman code length assigned to a given symbol.
type CodeLengthList []uint8

// Transforms a slice of CodeSymbol ranges to a slice, mapping a symbol from
// the original alphabet (index) to code's bit-length.
// codeLengths[ symbol ] = codeLength
func convertCodeSymbolRangesToCodeList(csRanges []FixedCodeSymbolRange) CodeLengthList {
	lastSymbol := csRanges[len(csRanges)-1].LastSymbol()
	codeLengths := make([]uint8, lastSymbol, lastSymbol)
	for _, v := range csRanges {
		for symbol := v.StartSymbol; symbol < v.LastSymbol(); symbol++ {
			codeLengths[symbol] = v.CodeLength
		}
	}
	return codeLengths
}

// Represents a RunLength decomp. algorithm symbol. These define the tables as
// in DEFLATE rfc1951:3.2.5
type ExtraBits struct {
	ExtraBits uint8
	BaseValue uint16
}

// Represents Alphabet details for runlength decompression.
type RunLengthAlphabet map[Symbol]ExtraBits

func (r ExtraBits) Value(extraBits uint64) uint16 {
	return r.BaseValue + uint16(extraBits)
}

func getLengthExtraBits() RunLengthAlphabet {
	return RunLengthAlphabet{
		257: {0, 3},
		258: {0, 4},
		259: {0, 5},
		260: {0, 6},
		261: {0, 7},
		262: {0, 8},
		263: {0, 9},
		264: {0, 10},
		265: {1, 11},
		266: {1, 13},
		267: {1, 15},
		268: {1, 17},
		269: {2, 19},
		270: {2, 23},
		271: {2, 27},
		272: {2, 31},
		273: {3, 35},
		274: {3, 43},
		275: {3, 51},
		276: {3, 59},
		277: {4, 67},
		278: {4, 83},
		279: {4, 99},
		280: {4, 115},
		281: {5, 131},
		282: {5, 163},
		283: {5, 195},
		284: {5, 227},
		285: {0, 258},
	}
}

func getDistanceExtraBits() RunLengthAlphabet {
	return RunLengthAlphabet{
		0:  {0, 1},
		1:  {0, 2},
		2:  {0, 3},
		3:  {0, 4},
		4:  {1, 5},
		5:  {1, 7},
		6:  {2, 9},
		7:  {2, 13},
		8:  {3, 17},
		9:  {3, 25},
		10: {4, 33},
		11: {4, 49},
		12: {5, 65},
		13: {5, 97},
		14: {6, 129},
		15: {6, 193},
		16: {7, 257},
		17: {7, 385},
		18: {8, 513},
		19: {8, 769},
		20: {9, 1025},
		21: {9, 1537},
		22: {10, 2049},
		23: {10, 3073},
		24: {11, 4097},
		25: {11, 6145},
		26: {12, 8193},
		27: {12, 12289},
		28: {13, 16385},
		29: {13, 24577},
	}
}
