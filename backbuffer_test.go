package gogz

import (
	"testing"
)

func TestIfItsPossibleToWriteToWriteAndReadFromBackBuffer(t *testing.T) {
	data := []byte("some random data")

	b, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("failed to instantiate back buffer: %v\n", err)
	}

	n, err := b.Write(data)
	if err != nil {
		t.Fatalf("failed to write to back buffer: %v\n", err)
	}

	if n != len(data) {
		t.Fatalf("expected to write %d bytes, written: %d\n", n, len(data))
	}
}

func TestIfItsPossibleToRetrieveHistoryUpToConfiguredSize(t *testing.T) {
	testData := [][]byte{
		[]byte("some random data"),
		[]byte("xxx"),
		[]byte("blah blah blah"),
	}

	for _, expected := range testData {
		expectedLen := len(expected)
		gotLen := expectedLen / 3
		got := make([]byte, gotLen)
		b, _ := NewBackBuffer(expectedLen * 2)
		b.Write(expected)

		historyLen := b.Len()
		offset := historyLen - gotLen
		n, err := b.ReadAt(got, int64(offset))
		if err != nil {
			t.Fatalf("unexpected error during ReadAt: %v\n", err)
		}

		if n != gotLen {
			t.Fatalf("expected to get: %d bytes, got: %d\n", gotLen, n)
		}

		for i := range expected[offset:] {
			if expected[i+offset] != got[i] {
				t.Fatalf("discrepant data, expected %x(%c)@%d, got %x(%c)\n",
					expected[i], expected[i], i, got[i], got[i])
			}
		}
	}
}

func TestIfFlushesDataBeyondConfiguredMaxSize(t *testing.T) {
	testData := [][]byte{
		[]byte("some random data"),
		[]byte("xxx"),
		[]byte("blah blah blah"),
	}

	for _, expected := range testData {
		expectedLen := len(expected)
		got := make([]byte, expectedLen)
		b, _ := NewBackBuffer(expectedLen * 2)
		b.Write(expected)

		n, err := b.Read(got)
		if err != nil {
			t.Fatalf("failed to read from back buffer: %v\n", err)
		}

		if n != expectedLen {
			t.Fatalf("expected to read %d bytes, read: %d\n", n, len(got))
		}

		for i := range expected {
			if expected[i] != got[i] {
				t.Fatalf("data different @%d %x != %x\n", i, expected[i], got[i])
			}
		}
	}
}
