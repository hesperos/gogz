package gogz

import (
	"bytes"
	"fmt"
	"io"
)

type BackBufferReaderWriter interface {
	io.Reader
	io.ReaderAt
	io.Writer
	Len() int
}

type BackBuffer struct {
	MaxSize int
	buffer  bytes.Buffer
}

func (b *BackBuffer) Read(p []byte) (int, error) {
	return b.buffer.Read(p)
}

func (b *BackBuffer) Len() int {
	return b.buffer.Len()
}

func (b *BackBuffer) ReadAt(p []byte, off int64) (int, error) {
	bufLen := b.buffer.Len()
	if off >= int64(bufLen) {
		return 0, fmt.Errorf("ivalid offset %d, past buffer size: %d\n", off, bufLen)
	}
	return copy(p, b.buffer.Bytes()[off:]), nil
}

func (b *BackBuffer) Write(p []byte) (n int, err error) {
	n, err = b.buffer.Write(p)
	toStrip := b.buffer.Len() - b.MaxSize
	if toStrip > 0 {
		b.buffer.Truncate(b.MaxSize)
	}
	return
}

func NewBackBuffer(maxSize int) (*BackBuffer, error) {
	backBuffer := BackBuffer{
		MaxSize: maxSize,
	}

	// Grow capacity to avoid re-allocations. Chosen size is twice the max
	// size, to accommodate for spikes.
	backBuffer.buffer.Grow(2 * maxSize)

	return &backBuffer, nil
}
