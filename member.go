package gogz

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"hash"
	"hash/crc32"
	"io"
	"io/ioutil"
	"strings"
	"time"
)

type Os uint8

type CompressionMethod uint8

type Magic uint8

const (
	Id1 Magic = 0x1f
	Id2 Magic = 0x8b
)

const (
	CmDeflate CompressionMethod = 8
)

const (
	OsFat Os = iota
	OsAmiga
	OsVms
	OsUnix
	OsVmCms
	OsAtariTos
	OsHpfs
	OsMacintosh
	OsZSystem
	OsCpm
	OsTops20
	OsNtfs
	OsQDos
	OsRiscos
	OsUnknown
)

type BlockType int

const (
	BlockUncompressed BlockType = iota
	BlockCompressedFixed
	BlockCompressedDynamic
	BlockReserved
)

type Flags struct {
	Flags uint8
}

func (f Flags) IsText() bool {
	return (f.Flags&(1<<0x00) != 0x00)
}

func (f Flags) HasCrc16() bool {
	return (f.Flags&(1<<0x01) != 0x00)
}

func (f Flags) HasExtraFields() bool {
	return (f.Flags&(1<<0x02) != 0x00)
}

func (f Flags) HasFileName() bool {
	return (f.Flags&(1<<0x03) != 0x00)
}

func (f Flags) HasComment() bool {
	return (f.Flags&(1<<0x04) != 0x00)
}

type Counter struct {
	WrittenBytes uint
	ReadBytes    uint
}

func (c *Counter) Read(p []byte) (int, error) {
	c.ReadBytes += uint(len(p))
	return len(p), nil
}

func (c *Counter) Write(p []byte) (int, error) {
	c.WrittenBytes += uint(len(p))
	return len(p), nil
}

type MemberHeader struct {
	CompressionMethod CompressionMethod
	Flg               Flags
	ModificationTime  time.Time
	Xfl               uint8
	Os                Os
	FileName          string
	Comment           string
	HeaderCrc16       uint16
}

type MemberFooter struct {
	Crc32     uint32
	InputSize uint32
}

type Member struct {
	Header *MemberHeader
	Footer *MemberFooter

	backBuffer    BackBufferReaderWriter
	stagingBuffer bytes.Buffer
	staging       io.Writer
	counter       Counter
	crc32         hash.Hash32

	bitReader *BitReader
}

func isGzMemberHeader(r io.Reader) (bool, error) {
	var ids [2]uint8
	n, err := io.ReadFull(r, ids[:])

	if err != nil {
		return false, err
	}
	if n != len(ids) {
		return false, fmt.Errorf("unable to read gz header\n")
	}
	if Magic(ids[0]) != Id1 || Magic(ids[1]) != Id2 {
		return false, fmt.Errorf("invalid gz header found\n")
	}

	return true, nil
}

func ReadString(r io.Reader) (string, error) {
	buffer := bytes.Buffer{}
	c := make([]byte, 1, 2)

	for {
		n, err := r.Read(c)
		if err != nil {
			return "", err
		}

		if n != len(c) {
			return "", fmt.Errorf("expected to read %d, read %d\n", len(c), n)
		}

		if c[0] == 0x00 {
			// we're done here
			break
		}
		buffer.WriteByte(c[0])
	}
	return buffer.String(), nil
}

func NewMemberHeader(r io.Reader) (*MemberHeader, error) {
	var mtime uint32 = 0
	header := MemberHeader{}

	if ok, err := isGzMemberHeader(r); !ok {
		return nil, err
	}

	if err := binary.Read(r, binary.LittleEndian, &header.CompressionMethod); err != nil {
		return nil, fmt.Errorf("unable to get the compression-method: %v\n", err)
	}

	if err := binary.Read(r, binary.LittleEndian, &header.Flg.Flags); err != nil {
		return nil, fmt.Errorf("unable to get the member flags: %v\n", err)
	}

	if err := binary.Read(r, binary.LittleEndian, &mtime); err != nil {
		return nil, fmt.Errorf("unable to get the modification-time: %v\n", err)
	}
	header.ModificationTime = time.Unix(int64(mtime), 0)

	if err := binary.Read(r, binary.LittleEndian, &header.Xfl); err != nil {
		return nil, fmt.Errorf("unable to get the xfl: %v\n", err)
	}

	if err := binary.Read(r, binary.LittleEndian, &header.Os); err != nil {
		return nil, fmt.Errorf("unable to get the os: %v\n", err)
	}

	if header.Flg.HasExtraFields() {
		// this is not supported, just skip this part if it's present
		var xlen uint16 = 0
		if err := binary.Read(r, binary.LittleEndian, &xlen); err != nil {
			return nil, fmt.Errorf("unable to get the xlen: %v\n", err)
		}
		io.CopyN(ioutil.Discard, r, int64(xlen))
	}

	if header.Flg.HasFileName() {
		fname, err := ReadString(r)
		if err != nil {
			return nil, fmt.Errorf("unable to get the filename: %v\n", err)
		}
		header.FileName = strings.TrimRight(fname, "\x00")
	}

	if header.Flg.HasComment() {
		comment, err := ReadString(r)
		if err != nil {
			return nil, fmt.Errorf("unable to get the comment: %v\n", err)
		}
		header.Comment = strings.TrimRight(comment, "\x00")
	}

	if header.Flg.HasCrc16() {
		if err := binary.Read(r, binary.LittleEndian, header.HeaderCrc16); err != nil {
			return nil, fmt.Errorf("unable to get the xlen: %v\n", err)
		}
	}

	return &header, nil
}

func readBlockHeader(b *BitReader) (BlockType, bool, error) {
	isFinal, err := b.ReadBits(1)
	if err != nil {
		return BlockUncompressed, false, err
	}

	blockType, err := b.ReadBits(2)
	if err != nil {
		return BlockUncompressed, false, err
	}

	return BlockType(blockType), isFinal == 1, nil
}

func (m *Member) readUncompressedBlock() error {
	m.bitReader.Flush()
	var dataLen uint16 = 0
	var dataNLen uint16 = 0

	if err := binary.Read(m.bitReader, binary.LittleEndian, &dataLen); err != nil {
		return fmt.Errorf("unable to get the dataLen: %v\n", err)
	}
	if err := binary.Read(m.bitReader, binary.LittleEndian, &dataNLen); err != nil {
		return fmt.Errorf("unable to get the dataNLen: %v\n", err)
	}

	if dataLen != ^dataNLen {
		return fmt.Errorf("dataLen(%04x) != dataNLen(%04x)\n", dataLen, dataNLen)
	}

	io.CopyN(m.staging, m.bitReader, int64(dataLen))
	return nil
}

func (m *Member) readCompressedBlock(litLenLookup, distanceLookup HuffmanLookup) error {
	llMinLen, llMaxLen := getMinMaxHuffmanCodeLength(litLenLookup)
	distMinLen, distMaxLen := getMinMaxHuffmanCodeLength(distanceLookup)
	isEndOfBlock := false

	extraLenTable := getLengthExtraBits()
	extraDistTable := getDistanceExtraBits()
	repeats := make([]byte, 0, 1024)

	for !isEndOfBlock {
		symbol, err := matchHuffmanCode(m.bitReader, litLenLookup, llMinLen, llMaxLen)
		if err != nil {
			return err
		}

		switch {
		case symbol < 256:
			m.staging.Write([]uint8{uint8(symbol)})

		case symbol == 256:
			isEndOfBlock = true

		default:
			extraLenBits, ok := extraLenTable[symbol]
			if !ok {
				return fmt.Errorf("unknown symbol - doesn't represent a length: [%x]\n", symbol)
			}

			extraLenBitsVal, err := m.bitReader.ReadBits(uint(extraLenBits.ExtraBits))
			if err != nil {
				return err
			}

			// we've got the length to be repeated
			length := extraLenBits.Value(extraLenBitsVal)

			// proceed with obtaining information about the distance
			symbol, err := matchHuffmanCode(m.bitReader, distanceLookup, distMinLen, distMaxLen)
			if err != nil {
				return err
			}

			extraDistBits, ok := extraDistTable[symbol]
			if !ok {
				return fmt.Errorf("unknown symbol - doesn't represent a distance: [%x]\n", symbol)
			}

			extraDistBitsVal, err := m.bitReader.ReadBits(uint(extraDistBits.ExtraBits))
			if err != nil {
				return err
			}

			// we've got the distance to look back to
			distance := extraDistBits.Value(extraDistBitsVal)

			// obtain the data
			repeats = repeats[:length]
			offset := int64(m.backBuffer.Len()) - int64(distance)
			n, err := m.backBuffer.ReadAt(repeats, offset)
			if err != nil {
				return fmt.Errorf("failed to perform backwards lookup: %v\n", err)
			}
			repeats = repeats[:n]

			// replicate the data
			for nRepeats := length / distance; nRepeats > 0; nRepeats-- {
				m.staging.Write(repeats)
			}

			if nRemaining := length % distance; nRemaining > 0 {
				repeats = repeats[:nRemaining]
				n, err := m.backBuffer.ReadAt(repeats, offset)
				if int(nRemaining) != n || err != nil {
					return fmt.Errorf("failed to perform backwards lookup: %v\n", err)
				}
				m.staging.Write(repeats)
			}
		}
	}

	return nil
}

func (m *Member) VerifyFooter() error {
	if m.Footer.InputSize != uint32(m.counter.WrittenBytes) {
		return fmt.Errorf("length mismatch. decompressed: %d, expected: %d\n",
			uint32(m.counter.WrittenBytes),
			m.Footer.InputSize)
	}

	if m.Footer.Crc32 != m.crc32.Sum32() {
		return fmt.Errorf("crc32 mismatch: %x, expected: %x\n",
			m.crc32.Sum32(),
			m.Footer.Crc32)
	}

	return nil
}

func readHuffmanTables(bitReader *BitReader) (HuffmanLookup, HuffmanLookup, error) {
	// read the number of literal/length codes (257 - 286)
	hlit, err := bitReader.ReadBits(5)
	if err != nil {
		return HuffmanLookup{}, HuffmanLookup{}, err
	}

	// read the number of distance codes (1 - 32)
	hdist, err := bitReader.ReadBits(5)
	if err != nil {
		return HuffmanLookup{}, HuffmanLookup{}, err
	}

	// read the number of code lengths for the decompression alphabet (4 - 19)
	hclen, err := bitReader.ReadBits(4)
	if err != nil {
		return HuffmanLookup{}, HuffmanLookup{}, err
	}

	hlit += 257
	hdist += 1
	hclen += 4

	// Decompression alphabet contains symbols: 0 - 18
	// Decompression alphabet is an alphabet used to compress
	// the literal/length and distance Huffman code-lengths -
	// in order to further save space. The literal/length and
	// distance tables are recreated using a special
	// algorithm as described in rfc1951.txt:3.2.7.
	decompAlphaCodeLengthList := make([]uint8, 19)
	decompAlphaKeysOrder := []uint8{
		16, 17, 18, 0,
		8, 7, 9, 6,
		10, 5, 11, 4,
		12, 3, 13, 2,
		14, 1, 15,
	}

	// read the lengths of the Huffman codes used to encode
	// the decompression alphabet
	for i := 0; i < int(hclen); i++ {
		codeLen, err := bitReader.ReadBits(3)
		if err != nil {
			return HuffmanLookup{}, HuffmanLookup{}, err
		}
		decompAlphaCodeLengthList[decompAlphaKeysOrder[i]] = uint8(codeLen)
	}

	decompAlphaLookup := GeneratHuffmanLookupFromCodeLengthList(decompAlphaCodeLengthList)
	minLen, maxLen := getMinMaxHuffmanCodeLength(decompAlphaLookup)
	huffmanCodeLenList := make([]uint8, 0, hlit+hdist)

	for i := hlit + hdist; i > 0; {
		symbol, err := matchHuffmanCode(bitReader, decompAlphaLookup, minLen, maxLen)
		if err != nil {
			return HuffmanLookup{}, HuffmanLookup{}, err
		}

		var repeats uint64 = 0
		var codeLen uint64 = 0

		switch {
		case symbol >= 0 && symbol <= 15:
			repeats = 1
			codeLen = uint64(symbol)

		case symbol == 16:
			repeats, _ = bitReader.ReadBits(2)
			repeats += 3
			codeLen = uint64(huffmanCodeLenList[len(huffmanCodeLenList)-1])

		case symbol == 17:
			repeats, _ = bitReader.ReadBits(3)
			repeats += 3

		case symbol == 18:
			repeats, _ = bitReader.ReadBits(7)
			repeats += 11

		default:
		}

		// fill in the code length list
		for n := 0; n < int(repeats); n++ {
			huffmanCodeLenList = append(huffmanCodeLenList, uint8(codeLen))
		}

		i -= repeats
	}

	// generate the lookup tables
	litLenLookup := GeneratHuffmanLookupFromCodeLengthList(huffmanCodeLenList[:hlit])
	distanceLookup := GeneratHuffmanLookupFromCodeLengthList(huffmanCodeLenList[hlit:])

	return litLenLookup, distanceLookup, nil
}

func (m *Member) Read(p []byte) (int, error) {
	for m.stagingBuffer.Len() < len(p) && m.Footer == nil {
		blockType, isFinal, err := readBlockHeader(m.bitReader)
		if err != nil {
			return 0, err
		}

		switch blockType {
		case BlockUncompressed:
			m.readUncompressedBlock()

		case BlockCompressedFixed:
			litLenCodeList := convertCodeSymbolRangesToCodeList(
				getFixedCodeLitLenSymbolRanges())
			distanceCodeList := convertCodeSymbolRangesToCodeList(
				getFixedCodeDistanceSymbolRanges())

			litLenLookup :=
				GeneratHuffmanLookupFromCodeLengthList(litLenCodeList)
			distanceLookup :=
				GeneratHuffmanLookupFromCodeLengthList(distanceCodeList)

			m.readCompressedBlock(litLenLookup, distanceLookup)

		case BlockCompressedDynamic:
			litLenLookup, distanceLookup, err := readHuffmanTables(m.bitReader)
			if err != nil {
				return 0, err
			}
			m.readCompressedBlock(litLenLookup, distanceLookup)
		}

		if isFinal {
			footer, err := NewMemberFooter(m.bitReader)
			if err != nil {
				return 0, err
			}
			m.Footer = footer
			err = m.VerifyFooter()
			if err != nil {
				return 0, err
			}
			break
		}
	}

	return m.stagingBuffer.Read(p)
}

func NewMemberFooter(r io.Reader) (*MemberFooter, error) {
	footer := MemberFooter{}

	if err := binary.Read(r, binary.LittleEndian, &footer.Crc32); err != nil {
		return nil, fmt.Errorf("unable to get the crc32: %v\n", err)
	}

	if err := binary.Read(r, binary.LittleEndian, &footer.InputSize); err != nil {
		return nil, fmt.Errorf("unable to get the input-size: %v\n", err)
	}

	return &footer, nil
}

func NewMember(r io.Reader, b BackBufferReaderWriter) (*Member, error) {
	bitReader, err := NewBitReader(r)
	if err != nil {
		return nil, fmt.Errorf("failed to create bit reader\n")
	}

	header, err := NewMemberHeader(bitReader)
	if err != nil {
		return nil, err
	}

	member := Member{
		Header:     header,
		backBuffer: b,
		bitReader:  bitReader,
		crc32:      crc32.NewIEEE(),
	}

	member.staging = io.MultiWriter(member.crc32,
		member.backBuffer,
		&member.stagingBuffer,
		&member.counter)

	return &member, nil
}
