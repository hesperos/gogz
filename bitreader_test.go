package gogz

import (
	"bytes"
	"io"
	"testing"
)

func TestIfItsPossibleToConstructSuccessfully(t *testing.T) {
	dataSlice := []byte{1, 2, 3, 4, 5, 6, 7, 8}
	bitReader, err := NewBitReader(bytes.NewReader(dataSlice))
	if err != nil {
		t.Fatalf("unable to create an instance of bit reader: %v\n", err)
	}

	if bitReader == nil {
		t.Fatalf("no error reported but returned instance is nil\n")
	}
}

func TestIfItsPossibleToObtainFullBytes(t *testing.T) {
	dataSlice := []byte{1, 2, 3, 4, 5, 6, 7, 8}
	bitReader, _ := NewBitReader(bytes.NewReader(dataSlice))

	for _, v := range dataSlice {
		bits, err := bitReader.ReadBits(8)
		if err != nil {
			t.Fatalf("unable to read data: %v\n", err)
		}

		if uint64(v) != bits {
			t.Fatalf("expected %x, got %x\n", v, bits)
		}
	}
}

func TestIfItsPossibleToObtainPartialBytes(t *testing.T) {
	dataSlice := []byte{1, 2, 3, 4, 5, 6, 7, 8}
	bitReader, _ := NewBitReader(bytes.NewReader(dataSlice))

	testData := []struct {
		Length   uint
		Expected uint64
	}{
		{4, 0x01},
		{5, 0x00},
		{6, 0x01},
		{8, 0x06},
		{9, 0x08},
		{1, 0x01},
		{2, 0x02},
		{3, 0x00},
		{12, 0xc18},
		{6, 0x01},
		{6, 0x08},
		{2, 0x00},
	}

	for _, v := range testData {
		bits, err := bitReader.ReadBits(v.Length)
		if err != nil {
			t.Fatalf("unable to read %d bits of data: %v\n", v.Length, err)
		}

		if v.Expected != bits {
			t.Fatalf("expected %x, got %x\n", v.Expected, bits)
		}
	}
}

func TestIfReadingPassEnfOfStreamReturnsEOF(t *testing.T) {
	dataSlice := []byte{1, 2, 3, 4, 5, 6, 7, 8}
	bitReader, _ := NewBitReader(bytes.NewReader(dataSlice))

	// purge it
	for range dataSlice {
		bitReader.ReadBits(8)
	}

	data, err := bitReader.ReadBits(1)
	if data != 0 {
		t.Fatalf("expected no data, got: %x\n", data)
	}

	if err != io.EOF {
		t.Fatalf("expected io.EOF, got %v\n", err)
	}
}

func TestIfFlushPurgesAccumulatedData(t *testing.T) {
	dataSlice := []byte{1, 2, 3, 4, 5, 6, 7, 8}
	bitReader, _ := NewBitReader(bytes.NewReader(dataSlice))

	bitReader.ReadBits(10)

	if bitReader.Available == 0 {
		t.Fatalf("expected some buffered data to exist\n")
	}

	bitReader.Flush()
	if bitReader.Available != 0 {
		t.Fatalf("expected buffered data to be flushed got: %d, %x\n", bitReader.Available, bitReader.Data)
	}

}
