package gogz

import (
	"os"
	"testing"
)

func TestIfItsPossibleToCreateAReader(t *testing.T) {
	inputFh, err := os.Open("testdata/rfc1952.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	reader, err := NewReader(inputFh)
	if err != nil {
		t.Fatalf("error when creating gogz Reader: %s\n", err)
	}

	defer reader.Close()
}
