package gogz

import (
	"io"
)

const MaxDistance = 32768

type GoGzReader struct {
	origReader    io.Reader
	backBuffer    *BackBuffer
	currentMember *Member
}

func (r *GoGzReader) SetupMemberReader() (*Member, error) {
	member, err := NewMember(r.origReader, r.backBuffer)
	if err != nil {
		return nil, err
	}
	r.currentMember = member
	return member, nil
}

func (r *GoGzReader) Read(p []byte) (int, error) {
	if r.currentMember == nil {
		if _, err := r.SetupMemberReader(); err != nil {
			return 0, err
		}
	}
	return r.currentMember.Read(p)
}

func (r *GoGzReader) Close() error {
	return nil
}

func NewReader(r io.Reader) (*GoGzReader, error) {
	const MaxLookbackDistance = 32768
	backBuffer, err := NewBackBuffer(MaxLookbackDistance)
	if err != nil {
		return nil, err
	}

	reader := GoGzReader{
		origReader: r,
		backBuffer: backBuffer,
	}

	if _, err := reader.SetupMemberReader(); err != nil {
		return nil, err
	}

	return &reader, nil
}
