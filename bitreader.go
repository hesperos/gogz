package gogz

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type BitReader struct {
	r         io.Reader
	Available uint
	Data      uint64
	tmp       []uint8
}

func (b *BitReader) Flush() {
	b.Available = 0
	b.Data = 0
}

func (b *BitReader) Read(p []byte) (int, error) {
	b.Flush()
	return b.r.Read(p)
}

func (b *BitReader) ReadBits(n uint) (uint64, error) {
	var rv uint64 = 0
	if n > 64 {
		return 0, fmt.Errorf("unable to provide more than 64 bits in a single request\n")
	}

	for b.Available < n {
		_, err := b.r.Read(b.tmp)

		if err != nil {
			switch err {
			case io.EOF:
				return 0, err
			default:
				return 0, fmt.Errorf("unable to cache data: %v\n", err)
			}
		}

		b.Data |= uint64(b.tmp[0]) << b.Available
		b.Available += 8
	}

	rv = b.Data & ((0x01 << n) - 1)
	b.Data >>= n
	b.Available -= uint(n)
	return rv, nil
}

func NewBitReader(r io.Reader) (*BitReader, error) {
	reader := BitReader{
		r:         bufio.NewReaderSize(r, 32*os.Getpagesize()),
		Available: 0,
		Data:      0,
		tmp:       make([]uint8, 1),
	}
	return &reader, nil
}
